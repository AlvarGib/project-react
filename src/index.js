import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
import './index.scss';

import store from './app/store';
import { Provider } from 'react-redux';

import App from './App';


firebase.initializeApp({
    apiKey: "AIzaSyBKXZ8clkAF6yAitVeKCZMDc_l2du6Btog",
    authDomain: "react-proyect-ac326.firebaseapp.com",
    projectId: "react-proyect-ac326",
    storageBucket: "react-proyect-ac326.appspot.com",
    messagingSenderId: "204140507464",
    appId: "1:204140507464:web:ac11e0a6c8c61c6eeac37d",
    measurementId: "G-51E5S5NVB8"
});


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
