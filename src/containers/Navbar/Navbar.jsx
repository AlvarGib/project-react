import React from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faFeather } from '@fortawesome/free-solid-svg-icons'
import './Navbar.scss'
import Form from '../Form/Form';
import Form2 from '../../components/Formulario2/Form2';

const Navbar = () => {
  return (
    <Router>
      <nav className="navbar">
        <div className="navbar__left">
          <img className="navbar__left__imagen" src="https://static.www.nfl.com/t_q-best/league/api/clubs/logos/PHI" alt="" />
          <div className="navbar__left__item">
            <a className="navbar__left__item" href="/">
              <FontAwesomeIcon icon={faHome} white />
            </a>
          </div>
          <div className="navbar__left__item">
            <input className="navbar__left__item__input" />
          </div>
        </div>
        <div className="link">
          <Form2 />
          <Link to="/form" className="link">
            Reguistrate <FontAwesomeIcon icon={faFeather} />
          </Link>
        </div>
      </nav>
      <Switch>
        <Route path="/form">
          <Form />
        </Route>
      </Switch>
    </Router>
  )
}

export default connect()(Navbar);
