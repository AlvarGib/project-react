import React, { useState } from 'react';

import FormSignup from '../../components/FormSignup/FormSignup';
import FormSuccess from '../../components/FormSucces/FormSuccess';



import './Form.scss';


const Form = () => {

    
    const [isSubmitted, setIsSubmitted] = useState(false)

    function submitForm() {
        setIsSubmitted(true);

    }
    return (
        <>
            <div className="form-container">
                <span className="close-btn"></span>
                <div className="form-content-left">
                    <img src="https://static.www.nfl.com/t_q-best/league/api/clubs/logos/PHI" alt=""/>
                </div>
                {!isSubmitted ? (<FormSignup submitForm={submitForm} />) : (<FormSuccess />)}
            </div>
        </>
    )
}

export default Form
