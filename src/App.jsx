import React from 'react';
import { Provider } from 'react-redux';


import store from './store.jsx';
import './App.scss';
import Jugadores from './containers/Jugadores/Jugadores.jsx';
import EquipoSeleccionado from './components/EquipoSeleccionado/EquipoSeleccionado.jsx';
import Navbar from './containers/Navbar/Navbar.jsx';
import Footer from './components/Footer/Footer.jsx';
import Errores from './components/Errores/Errores.jsx';
import FileUpload from "./components/FileUpload/FileUpload.jsx";



const App = () => {
  return (
    <Provider store={store}>
      <main className="app">
        <Navbar />
        <Jugadores />
        <Errores />
        <EquipoSeleccionado />
        <FileUpload/>
      </main>
      <div className="footer">
        <Footer />
      </div>
    </Provider>
  );
}

export default App;


