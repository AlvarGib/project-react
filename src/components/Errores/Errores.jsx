import React from 'react';
import { connect } from 'react-redux';

import "./Errores.scss";

const Errores = ({ errores }) => {

    return (
        <div className="">
            {
                errores.map(j => (
                    <article className='titular' key={j.id}>
                        <div className="error">
                            <p>No se puede repetir esta posición</p>
                        </div>
                    </article>
                ))
            }
        </div>
    )

}

const mapStateToProps = state => ({

    errores: state.errores

})

const mapDispatchToProps = dispatch => ({
    agregarTitular(jugador) {
        dispatch({
            type: "AGREGAR_TITULAR",
            jugador
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Errores)