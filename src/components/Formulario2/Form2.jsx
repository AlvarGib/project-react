import React, { Component } from 'react'
import firebase from 'firebase';


import "./Form2.scss"

class Form2 extends Component {

    constructor() {
        super();
        this.state = {
            user: null
        };

        this.handleAuth = this.handleAuth.bind(this);
        this.handleLogout= this.handleLogout.bind(this);
    }

    componentWillMount(){
        firebase.auth().onAuthStateChanged(user =>{
            this.setState({user});
        });
    }


    handleAuth() {
        const provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(provider)
            .then(result => console.log(`${result.user.email} ha iniciado sesion`))
            .cath(error => console.log(`Error ${error.code}: ${error.message}`))
    }

    handleLogout(){
        firebase.auth().signOut()
            .then(result => console.log(`${result.user.email} ha salido`))
            .cath(error => console.log(`Error ${error.code}: ${error.message}`))
    }

    renderLoginButton() {
        if (this.state.user) {
            return(
                <div>
                    <img src={this.state.user.photoURL} alt={this.state.user.displayName}/>
                    <p>Hola {this.state.user.displayName}</p>
                    <button onClick={this.handleLogout}> Salir</button>
                </div>
            )
        } else {
            return( 
            <button onClick={this.handleAuth}>
                    Login with Google
                </button>)
        }
    }

    render() {
        return (
            <div className="login">
                <p>
                    {this.renderLoginButton()}
                </p>
            </div>
        )
    }

}

export default Form2
