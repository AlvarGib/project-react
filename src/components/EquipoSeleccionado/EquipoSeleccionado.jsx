import React from 'react';
import Suplentes from '../Suplentes/Suplentes';
import Titulares from '../Titulares/Titulares';


const EquipoSeleccionado = () => (

    <section>
        <Titulares/>
        <Suplentes/>
    </section>
    
)

export default EquipoSeleccionado;