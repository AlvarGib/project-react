import React from 'react';
import { connect } from 'react-redux';

const Titulares = ({ titulares, quitatTitular}) => (
    <section>
        <h2>Titulares</h2>
        <div className="cancha">
            {
                titulares.map(j => (
                    <article className='titular' key={j.id}>
                        <div>
                            <img src={j.foto} alt={j.nombre}/>
                            <button onClick={() => quitatTitular(j)}>X</button>
                        </div>
                        <p>{j.nombre}</p>
                    </article>
                ) )
            }

            <img className="arena__imagen" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/AmFBfield.svg/1200px-AmFBfield.svg.png" alt="" />
        </div>
    </section>
)

const mapStateToProps = state => ({

    titulares: state.titulares

})

    const mapDispatchToProps = dispatch => ({
        quitatTitular(jugador){
            dispatch({
                type: "QUITAR_JUGADOR",
                jugador
            })
        }
    })

export default connect(mapStateToProps, mapDispatchToProps)(Titulares)