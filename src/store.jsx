import { createStore } from 'redux';


const initialState = {
    jugadores: [{
        id: 1,
        nombre: "CARSON WENTZ",
        pos: 1,
        dem: "QB",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/2573079.png&w=350&h=254"
    },
    {
        id: 2,
        nombre: "JALEN  HURTSS",
        pos: 1,
        dem: "QB",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/4040715.png&w=350&h=254"
    },
    {
        id: 3,
        nombre: "DESEAN JACKSON",
        pos: 3,
        dem: "Receptor 1",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/11283.png&w=350&h=254"
    },
    {
        id: 4,
        nombre: "ALSHON JEFFERY",
        pos: 4,
        dem: "Receptor 2",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/14912.png&w=350&h=254"
    },
    {
        id: 5,
        nombre: "J.J.ARCEGA-WHITESIDE",
        pos: 4,
        dem: "Receptor 2",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/3931397.png&w=350&h=254"
    },
    {
        id: 6,
        nombre: "JALEN REAGOR",
        pos: 6,
        dem: "Corredor",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/4241802.png&w=350&h=254"
    },
    {
        id: 7,
        nombre: "COREY CLEMENT",
        pos: 6,
        dem: "Corredor",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/3045260.png&w=350&h=254"
    },
    {
        id: 8,
        nombre: "MILES SANDERS",
        pos: 6,
        dem: "Corredor",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/4045163.png&w=350&h=254"
    },
    {
        id: 9,
        nombre: "JORDAN HOWARD",
        pos: 6,
        dem: "Corredor",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/3060022.png&w=350&h=254"
    },
    {
        id: 9,
        nombre: "M.ZACH ERTZD",
        pos: 7,
        dem: "tight end 1",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/15835.png&w=350&h=254"
    },
    {
        id: 9,
        nombre: "ALLAS GOEDERT",
        pos: 7,
        dem: "tight end 2",
        foto: "https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/3121023.png&w=350&h=254"
    },
    ],
    titulares: [],
    suplentes: [],
    errores: [],
}

function revisaPos(titulares) {

    if (titulares > 0) return false;
    return true;
}

function traducePos(pos) {
    switch (pos) {
        case 0: return "0";
        default: return "Qb";
    }
}

const reducerEntrenador = (state = initialState, action) => {

    if (action.type === "AGREGAR_TITULAR") {
        const repetidos = state.titulares.filter(t => t.pos === action.jugador.pos).length
        if (revisaPos(repetidos)) {
            return {
                ...state,
                titulares: state.titulares.concat(action.jugador),
                jugadores: state.jugadores.filter(j => j.id !== action.jugador.id),
                errores: []
            };
        }
        else {

            return {
                ...state,
                errores: state.errores.concat("No puedes tener dos jugadores con la posición " + traducePos(action.jugador.pos))
            }
        }


    }
    if (action.type === "AGREGAR_SUPLENTE") {
        return {
            ...state,
            suplentes: state.suplentes.concat(action.jugador),
            errores: [],
            jugadores: state.jugadores.filter(j => j.id !== action.jugador.id)
        };
    }
    if (action.type === "QUITAR_JUGADOR") {
        return {
            ...state,
            titulares: state.titulares.filter(j => j.id !== action.jugador.id),
            suplentes: state.suplentes.filter(j => j.id !== action.jugador.id),
            jugadores: state.jugadores.concat(action.jugador),
            errores: []
        };
    }
    return state
}

export default createStore(reducerEntrenador);